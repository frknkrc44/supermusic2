package org.frknkrc44.music2.holder;

public class MusicItem {
	
	public String title;
	public String artist;
	public String album;
	public String data;
	public String dispName;
	public int duration;
	public long songId;
	public long artistId;
	public long albumId;

	@Override
	public String toString(){
		return getClass().getSimpleName() +
				"{title: " + title +
				", artist: " + artist +
				", album: " + album +
				", data: " + data +
				", dispName: " + dispName +
				", duration: " + duration +
				", songId: " + songId +
				", artistId: " + artistId +
				", albumId: " + albumId + "}";
	}
	
}
