package org.frknkrc44.music2.util;

import android.database.*;
import android.provider.*;
import java.util.*;
import org.frknkrc44.music2.*;
import org.frknkrc44.music2.holder.*;
import android.os.*;

public class MusicListUtils {
	
	private MusicListUtils(){}
	
	private static final String UNKNOWN_TAG = "<unknown>";
	
	private static String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";

	private static String[] projection = {
		MediaStore.Audio.Media._ID,
		MediaStore.Audio.Media.ARTIST,
		MediaStore.Audio.Media.ARTIST_ID,
		MediaStore.Audio.Media.ALBUM,
		MediaStore.Audio.Media.ALBUM_ID,
		MediaStore.Audio.Media.TITLE,
		MediaStore.Audio.Media.DATA,
		MediaStore.Audio.Media.DISPLAY_NAME,
		MediaStore.Audio.Media.DURATION
	};
	
	private static final int INDEX_ID = 0,
							 INDEX_ARTIST = 1,
							 INDEX_ARTIST_ID = 2,
							 INDEX_ALBUM = 3,
							 INDEX_ALBUM_ID = 4,
							 INDEX_TITLE = 5,
							 INDEX_DATA = 6,
							 INDEX_DISPLAY_NAME = 7,
							 INDEX_DURATION = 8;
	
	public static void prepareMusicList(MusicListReadyListener listener){
		if(listener == null){
			return;
		}
		readyListener = listener;
		listener.start();
	}
	
	private static MusicListReadyListener readyListener;

	public static abstract class MusicListReadyListener {
		
		private void start(){
			new AsyncMusicListRetriever().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		}
		
		public abstract void onReady(MusicService.MusicList items);
	}
	
	private static class MusicTitleComparator implements Comparator<MusicItem>{
		
		private String capsFirstChar(String st){
			String temp = Character.toString(st.charAt(0)).toUpperCase();
			return temp + st.substring(1,st.length());
		}

		@Override
		public int compare(MusicItem p1, MusicItem p2){
			return capsFirstChar(p1.title).compareTo(capsFirstChar(p2.title));
		}
		
	}
	
	private static final MusicItem getItem(Cursor musicCursor){
		MusicItem item = new MusicItem();
		item.title = musicCursor.getString(INDEX_TITLE);
		item.artist = musicCursor.getString(INDEX_ARTIST);
		item.album = musicCursor.getString(INDEX_ALBUM);
		item.duration = musicCursor.getInt(INDEX_DURATION);
		item.songId = musicCursor.getLong(INDEX_ID);
		item.artistId = musicCursor.getLong(INDEX_ARTIST_ID);
		item.albumId = musicCursor.getLong(INDEX_ALBUM_ID);
		item.data = musicCursor.getString(INDEX_DATA);
		item.dispName = musicCursor.getString(INDEX_DISPLAY_NAME);
		if(item.album.equals(UNKNOWN_TAG)){
			item.album = "Unknown";
		}
		if(item.artist.equals(UNKNOWN_TAG)){
			item.artist = "Unknown";
		}
		return item;
	}
	
	public static final void sortList(ArrayList<MusicItem> list, boolean random){
		if(random){
			Collections.shuffle(list);
		} else {
			Collections.sort(list, new MusicTitleComparator());
		}
		
	}
	
	private static class AsyncMusicListRetriever extends AsyncTask<Void,Void,MusicService.MusicList> {

		@Override
		protected MusicService.MusicList doInBackground(Void... p1){
			MusicService.MusicList itemList = new MusicService.MusicList();
			Cursor musicCursor = MainApp.getResolver().query(
				MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
				projection,
				selection,
				null,
				null);
			musicCursor.moveToFirst();
			while(musicCursor.moveToNext()){
				/*if(musicCursor.getLong(INDEX_DURATION) < 10000){
					continue;
				}*/
				itemList.add(getItem(musicCursor));
			}
			sortList(itemList, false);
			return itemList;
		}

		@Override
		protected void onPostExecute(MusicService.MusicList result){
			readyListener.onReady(result);
			super.onPostExecute(result);
		}
		
	}
	
}
