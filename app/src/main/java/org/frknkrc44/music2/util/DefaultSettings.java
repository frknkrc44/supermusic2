package org.frknkrc44.music2.util;

public class DefaultSettings {
	
	private DefaultSettings(){}
	
	public static final int KEY_ICON_THEME = 0;
	
	public static final boolean KEY_LOAD_ALBUM_ART = true;
	public static final boolean KEY_SHOW_ALBUM_ART_ON_LOCK = true;
	
}
