package org.frknkrc44.music2.util;

import android.app.*;
import android.content.*;
import android.media.audiofx.*;
import android.provider.*;
import java.util.*;
import org.frknkrc44.music2.*;
import org.frknkrc44.music2.adapter.*;
import org.frknkrc44.music2.adapter.SettingsAdapter.*;

public class SettingMap extends LinkedHashMap<String,SettingType> {
	
	public static final String SET_KEY_ICON_THEME = "key_icon_theme",
	SET_KEY_LOAD_ALBUM_ART = "key_load_album_art",
	SET_KEY_SHOW_ALBUM_ART_ON_LOCK = "key_show_album_art_on_lock",
	SET_KEY_OPEN_EQUALIZER = "key_open_equalizer";
	
	public SettingMap(){
		put(SET_KEY_OPEN_EQUALIZER,SettingType.INTENT);
		put(SET_KEY_ICON_THEME,SettingType.SELECTOR);
		put(SET_KEY_LOAD_ALBUM_ART,SettingType.BOOL);
		put(SET_KEY_SHOW_ALBUM_ART_ON_LOCK,SettingType.BOOL);
	}
	
	public List<String> getSelectorItems(String key){
		switch(key){
			case SET_KEY_ICON_THEME:
				return ThemeAdapter.getIconThemes();
		}
		return null;
	}
	
	public static String getValueOrDefault(String key){
		return MainApp.getDatabase().getString(key,""+getDefault(key));
	}
	
	public static Object getDefault(String key){
		switch(key){
			case SET_KEY_ICON_THEME:
				return DefaultSettings.KEY_ICON_THEME;
			case SET_KEY_LOAD_ALBUM_ART:
				return DefaultSettings.KEY_LOAD_ALBUM_ART;
			case SET_KEY_SHOW_ALBUM_ART_ON_LOCK:
				return DefaultSettings.KEY_SHOW_ALBUM_ART_ON_LOCK;
		}
		return null;
	}
	
	public List<String> getAllKeys(){
		return new ArrayList<String>(keySet());
	}
	
	public CharSequence[] getSelectorItemsArray(String key){
		List<String> in = getSelectorItems(key);
		CharSequence[] csa = new CharSequence[in.size()];
		for(int i = 0;i < in.size();i++){
			csa[i] = in.get(i);
		}
		return csa;
	}
	
	public String getTranslation(String key){
		MainApp ctx = MainApp.getContext();
		String setKey = "settings_" + key;
		try {
			int id = ctx.getResources().getIdentifier(setKey, "string", ctx.getPackageName());
			if(id > 0)
				return ctx.getString(id);
		} catch(Throwable t){}
		return setKey;
	}
	
	public Intent getIntent(String key){
		switch(key){
			case SET_KEY_OPEN_EQUALIZER:
				Intent intent = new Intent(AudioEffect.ACTION_DISPLAY_AUDIO_EFFECT_CONTROL_PANEL);
				if(MainApp.getPackageMgr().resolveActivity(intent, 0) != null){
					intent.putExtra(AudioEffect.EXTRA_AUDIO_SESSION, MainApp.getAudioSessionId());
					intent.putExtra(AudioEffect.EXTRA_PACKAGE_NAME, MainApp.getContext().getPackageName());
					intent.putExtra(AudioEffect.EXTRA_CONTENT_TYPE, AudioEffect.CONTENT_TYPE_MUSIC);
					return intent;
				}
				return new Intent(Settings.ACTION_SOUND_SETTINGS);
		}
		return null;
	}
	
	public void startIntent(Context ctx, String key){
		switch(key){
			case SET_KEY_OPEN_EQUALIZER:
				((Activity)ctx).startActivityForResult(getIntent(key), 0);
				break;
		}
	}
	
}
