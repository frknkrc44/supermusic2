package org.frknkrc44.music2.util;

import android.content.res.*;
import android.graphics.*;
import android.os.*;
import android.text.*;
import android.view.*;
import android.widget.*;
import org.frknkrc44.music2.*;
import org.frknkrc44.music2.adapter.*;

public class NowPlayingUtils {
	
	private NowPlayingUtils(){}
	
	public static final void updateNowPlaying(IMusicService item, View nowPlayingView) throws Throwable {
		if(!nowPlayingView.isShown()){
			nowPlayingView.setVisibility(View.VISIBLE);
		}
		TextView text1 = nowPlayingView.findViewById(android.R.id.text1);
		TextView text2 = nowPlayingView.findViewById(android.R.id.text2);
		if(text1.getTag() != (int)0){
			text1.setGravity(Gravity.CENTER);
			text2.setGravity(text1.getGravity());
			text1.setEllipsize(TextUtils.TruncateAt.MARQUEE);
			text2.setEllipsize(text1.getEllipsize());
			text1.setMarqueeRepeatLimit(-1);
			text2.setMarqueeRepeatLimit(text1.getMarqueeRepeatLimit());
			text1.setSelected(true);
			text2.setSelected(true);
			text1.setTag(0);
		}
		text1.setText(item.getTitle());
		text2.setText(item.getArtist() + " - " + item.getAlbum());
		ImageButton playBtn = nowPlayingView.findViewById(android.R.id.button2);
		playBtn.setImageResource(ThemeAdapter.getPlayImageResource(item.isPlaying()));
		ImageButton prevBtn = nowPlayingView.findViewById(android.R.id.button1);
		prevBtn.setImageResource(ThemeAdapter.getPreviousImageResource());
		ImageButton nextBtn = nowPlayingView.findViewById(android.R.id.button3);
		nextBtn.setImageResource(ThemeAdapter.getNextImageResource());
		ImageButton setBtn = nowPlayingView.findViewById(android.R.id.edit);
		setBtn.setImageResource(ThemeAdapter.getSettingsImageResource());
		ImageView icon = nowPlayingView.findViewById(android.R.id.icon);
		Bitmap art = item.getAlbumArt();
		icon.setImageBitmap(art);
		setProgressTint(nowPlayingView, art);
	}
	
	public static final void updateProgress(View nowPlayingView, int progress){
		SeekBar prog = nowPlayingView.findViewById(android.R.id.empty);
		updateProgress(nowPlayingView, progress, prog.getMax());
	}
	
	public static final void updateProgress(View nowPlayingView, int progress, int maxProgress){
		SeekBar prog = nowPlayingView.findViewById(android.R.id.empty);
		prog.setMax(maxProgress);
		prog.setProgress(progress);
		TextView text1 = nowPlayingView.findViewById(R.id.position);
		TextView text2 = nowPlayingView.findViewById(R.id.duration);
		text1.setText(SongListAdapter.calculateTime(progress));
		text2.setText(SongListAdapter.calculateTime(maxProgress));
	}
	
	public static final void setProgressTint(View nowPlayingView, Bitmap albumArt){
		SeekBar prog = nowPlayingView.findViewById(android.R.id.empty);
		int color = BitmapUtils.getBitmapColor(albumArt);
		prog.setProgressTintList(ColorStateList.valueOf(color));
		prog.setThumbTintList(prog.getProgressTintList());
	}
	
}
