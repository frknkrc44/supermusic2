package org.frknkrc44.music2.adapter;
import java.util.*;
import org.frknkrc44.music2.*;
import org.frknkrc44.music2.util.*;

public class ThemeAdapter {
	
	private ThemeAdapter(){}
	
	public static int getShuffleImageResource(int mode){
		switch(getIconThemeMode()){
			case 1:
				switch(mode){
					case 1: return R.drawable.fl_ic_shuffle;
					default: return R.drawable.fl_ic_shuffle_off;
				}
			default:
				switch(mode){
					case 1: return R.drawable.lb_ic_shuffle;
					default: return R.drawable.lb_ic_shuffle_off;
				}
		}
	}
	
	public static int getLoopImageResource(int mode){
		switch(getIconThemeMode()){
			case 1:
				switch(mode){
					case 2: return R.drawable.fl_ic_loop_one;
					case 1: return R.drawable.fl_ic_loop;
					default: return R.drawable.fl_ic_loop_off;
				}
			default:
				switch(mode){
					case 2: return R.drawable.lb_ic_loop_one;
					case 1: return R.drawable.lb_ic_loop;
					default: return R.drawable.lb_ic_loop_off;
				}
		}
	}
	
	public static int getPlayImageResource(boolean isPlaying){
		switch(getIconThemeMode()){
			case 1: return isPlaying ? R.drawable.fl_ic_pause : R.drawable.fl_ic_play;
			default: return isPlaying ? R.drawable.lb_ic_pause : R.drawable.lb_ic_play;
		}
	}
	
	public static int getPreviousImageResource(){
		switch(getIconThemeMode()){
			case 1: return R.drawable.fl_ic_skip_previous;
			default: return R.drawable.lb_ic_skip_previous;
		}
	}
	
	public static int getNextImageResource(){
		switch(getIconThemeMode()){
			case 1: return R.drawable.fl_ic_skip_next;
			default: return R.drawable.lb_ic_skip_next;
		}
	}
	
	public static int getSettingsImageResource(){
		switch(getIconThemeMode()){
			case 1: return R.drawable.fl_ic_settings;
			default: return R.drawable.lb_ic_settings;
		}
	}
	
	private static int getIconThemeMode(){
		return Integer.parseInt(SettingMap.getValueOrDefault(SettingMap.SET_KEY_ICON_THEME));
	}
	
	public static List<String> getIconThemes(){
		List<String> out = new ArrayList<String>();
		out.add("Material");
		out.add("Flat");
		return out;
	}
	
}
