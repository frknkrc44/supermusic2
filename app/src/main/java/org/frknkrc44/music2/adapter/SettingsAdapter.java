package org.frknkrc44.music2.adapter;

import android.app.*;
import android.content.*;
import android.content.res.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import java.lang.reflect.*;
import java.util.*;
import org.blinksd.sdb.*;
import org.frknkrc44.music2.*;
import org.frknkrc44.music2.util.*;

public class SettingsAdapter {
	
	private SettingsAdapter(){}
	
	private static SettingMap sMap;
	
	static {
		sMap = new SettingMap();
	}
	
	private static View getSettingItemView(final Activity ctx, final String key){
		LinearLayout item = new LinearLayout(ctx);
		item.setLayoutParams(new LinearLayout.LayoutParams(-1,getListPreferredItemHeight(ctx)));
		int dp = (int)(ctx.getResources().getDisplayMetrics().density * 16);
		item.setPadding(dp,0,dp,0);
		setListItemBackground(item);
		switch(sMap.get(key)){
			case BOOL:
				Switch sv = new Switch(ctx);
				sv.setTextColor(0xFFDEDEDE);
				sv.setChecked(Boolean.parseBoolean(sMap.getValueOrDefault(key)));
				sv.setLayoutParams(new LinearLayout.LayoutParams(-1,-1));
				sv.setText(sMap.getTranslation(key));
				sv.setGravity(Gravity.CENTER_VERTICAL);
				sv.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener(){
					@Override
					public void onCheckedChanged(CompoundButton p1, boolean p2){
						putBoolean(key, p2);
					}	
				});
				item.addView(sv);
				break;
			case INT:
				break;
			case STRING:
				break;
			case INTENT:
			case SELECTOR:
				final TextView tv = new TextView(ctx);
				tv.setTextColor(0xFFDEDEDE);
				tv.setLayoutParams(new LinearLayout.LayoutParams(-1,-1));
				tv.setText(sMap.getTranslation(key));
				tv.setGravity(Gravity.CENTER_VERTICAL);
				item.addView(tv);
				item.setOnClickListener(new View.OnClickListener(){
					@Override
					public void onClick(View p1){
						switch(sMap.get(key)){
							case SELECTOR:
								AlertDialog.Builder build = new AlertDialog.Builder(p1.getContext());
								build.setTitle(tv.getText());
								build.setAdapter(new SelectorAdapter(p1.getContext(), key, sMap.getSelectorItems(key)), 
									new DialogInterface.OnClickListener(){

										@Override
										public void onClick(DialogInterface p1, int p2){
											putInt(key,p2);
											p1.dismiss();
										}

									});
								build.show();
								break;
							case INTENT:
								sMap.startIntent(p1.getContext(), key);
								break;
						}
						
					}
				});
				break;
		}
		return item;
	}
	
	private static int getListPreferredItemHeight(Context ctx){
		TypedValue value = new TypedValue();
		ctx.getTheme().resolveAttribute(android.R.attr.listPreferredItemHeight, value, true);
		return TypedValue.complexToDimensionPixelSize(value.data, ctx.getResources().getDisplayMetrics());
	}
	
	private static void setListItemBackground(View item){
		TypedArray arr = item.getContext().obtainStyledAttributes(new int[]{android.R.attr.selectableItemBackground});
		item.setBackgroundResource(arr.getResourceId(0,android.R.color.transparent));
		arr.recycle();
	}
	
	public static ViewGroup getSettingScreen(Activity ctx){
		ScrollView scroll = new ScrollView(ctx);
		scroll.setLayoutParams(new FrameLayout.LayoutParams(-1,-1));
		LinearLayout list = new LinearLayout(ctx);
		int dp = (int)(ctx.getResources().getDisplayMetrics().density * 16);
		list.setPadding(dp,dp,dp,dp);
		list.setOrientation(LinearLayout.VERTICAL);
		list.setLayoutParams(new LinearLayout.LayoutParams(-1,-1));
		scroll.addView(list);
		for(String key : sMap.getAllKeys()){
			list.addView(getSettingItemView(ctx,key));
		}
		return scroll;
	}
	
	private static void putBoolean(String key, boolean value){
		SuperMiniDB db = MainApp.getDatabase();
		db.putBoolean(key, value);
		db.writeKey(key);
	}
	
	private static void putInt(String key, int value){
		SuperMiniDB db = MainApp.getDatabase();
		db.putInteger(key, value);
		db.writeKey(key);
	}
	
	public enum SettingType {
		BOOL,
		INT,
		STRING,
		SELECTOR,
		INTENT
	}
	
	private static class SelectorAdapter extends ArrayAdapter<String> {
		
		private final int value;
		
		private SelectorAdapter(Context ctx, String key, List<String> items){
			super(ctx, android.R.layout.simple_list_item_single_choice, android.R.id.text1, items);
			value = Integer.parseInt(SettingMap.getValueOrDefault(key));
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent){
			if(convertView == null)
				convertView = super.getView(position, convertView, parent);
			CheckedTextView rb = convertView.findViewById(android.R.id.text1);
			setCheckMarkGravity(rb);
			rb.setChecked(value == position);
			return convertView;
		}
		
		private void setCheckMarkGravity(CheckedTextView ctv){
			try {
				Field f = CheckedTextView.class.getDeclaredField("mCheckMarkGravity");
				f.setAccessible(true);
				f.set(ctv,Gravity.START);
				ctv.invalidate();
			} catch(Throwable t){}
		}
		
	}
	
}
