package org.frknkrc44.music2.adapter;

import android.content.*;
import android.widget.*;
import org.frknkrc44.music2.holder.*;
import org.frknkrc44.music2.*;
import android.view.*;

public class SongListAdapter extends ArrayAdapter<MusicItem> {
	
	public SongListAdapter(Context ctx){
		super(ctx,R.layout.song_list_item,android.R.id.text1);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		if(convertView == null){
			convertView = super.getView(position, convertView, parent);
		}
		MusicItem item = getItem(position);
		TextView text = convertView.findViewById(android.R.id.text1);
		text.setText(item.title);
		text = convertView.findViewById(android.R.id.text2);
		text.setText(item.artist + " - " + item.album);
		text = convertView.findViewById(android.R.id.button1);
		text.setText(calculateTime(item.duration));
		return convertView;
	}
	
	public static String calculateTime(long duration){
		long secs = duration / 1000;
		long mins = secs / 60;
		secs %= 60;
		return addZero(mins) + ":" + addZero(secs);
	}
	
	private static String addZero(long num){
		return (num < 10 ? "0" : "") + num;
	}
	
}
